# Coding Standards

Conding rules

## Ground rules


1. ANY rule can be broken, if you KNOW what you are doing.
This rule comes from typography, where it is often said and repeated.

Rationale: the people who write the rules cannot know everything that will happen. There will be always grey zones, or exceptions. So it is better to document what is beheind the rule definition, and if in a particular case it does not apply, it can be skipped. Even penal law follows this principle: law evolves constantly, so being harder in a coding style as the penal law is the worst draconianism. 

The background of this rule is extremely profound and important: you can only know when to break a rule, if you know WHY the rule exists. So there are two corollaries:
a. If you do not know why the rule exists, do not break it
b. All rules should have its rationale properly documented. Corollary of that: any norm without rationales, should be deemed as incomplete, and taken as a rough guideline maximum.

2. Talking about style, that is, anything that can be automated by a linter, is a waste of time. 
You can of course define a style for the repository, but enforcing all programmers to respect it while coding, is a shooting ofence. Each programmer should be able to relint the code to his/her preference, work with it, and the lint in back and upload into the repository. 
Rationale:
Why bother the programmer with thousand of rules that can be easyly taken care of by software. After all, we are in the bussines of software. Let the computer do the work that it best does, let the programmer do the work he/she best does.

## Style

- A good style guide (as basis) is the google one: https://google.github.io/styleguide/cppguide.html
it is not perfect, but based on all the experience they have, must be a good starting point.
- Reasing "Clean code" by Robert C. Martin is also a very good starting point and should be done
- As said, all should be done with a linter
- Some very important topics are covered in famouse SICP course, which is EXTREMELY advised to be followed.

## Limits

One important method to achive good code is setting limitations. We propose here some of them:

1. Line length lines should be maximum 72 charcters wide, exceptionally 80
Long lines are difficult to read (look at any book about typography). Also sometimes lazyness make people just not scroll, and details are lost; ask me how I know. Also long lines could be an indication of too deep nesting, very long names, too much things done in only one line, etc. 
You can read more rationales in the book "Clean code"

2. Functions should be limited to 25 to 50 lines. Google says 40 lines maximum. Minimum 5 to 10 lines.
A function should do one thing, and one thing only. Functions should also deal with only one abstraction level. If both are respected, there should be no problem following this limit. I've never seen any algorithm in any book taking more than 50 steps, sometimes there are more total steps, but it is divided into different levels of abstraction, in which each one has much less steps.
If a function is only a couple of steps, it asks the question if the abstraction into a function is really needed.

a. Functions doing nothing, or almost nothing should be avoided. That include "setters and getters".
If you really want to HIDE the internal data structure, then you do not need setters and getters (which imply knowning something about the internals). The functions filling and polling status of an object must be part of an API or interface defined on the specific domain of the task or object being modeled.

b. Functions which are a long trivial list of repeating code are ok to be longer, as long as the language does not provided some reasonable mechanism as LISP macros

3. Variable names should be maximum 15 to 20 characters. Anything longer will make extremely difficult to even write consicely the most trivial code like:
a = b + c; without violating the maximum line width and being clear for the reader

4. In one access to a variable there should not be repeated selectors, and there should be maximum 3 of them.
You should work with levels of abstraction, and each level could deal at most with one level down of it. So accesing structures should be done only one step downwards.
For example, if you are working with the library foo, which has a member named bar inside, is ok to use "foo::bar", but accessing namespaces inside foo, like "foo::bar::type" would mean you have to have a deep understanding of the foo library from this level of abstraction, which is not good style. There should be in the library foo some whay of accesing things inside bar, if needed to use the foo library

foo::bar.data_struct->count is ok
bar->data_struct.count is ok
foo::bar::count is BAD
foo::bar.data_struct.count is BAD
bar->data_struct->count is BAD

5. Members per class

6. Classes per file

7. Files per module

9. 

----------
Text to be added:
 There are many coding style rules. Many of them are pretty good. Many
 are just trash. Very often I've  seen that people learn to program in
 one  language,  and  immediately  start  to  look  in  coding  styles
 documents,  and make  their own.  That is,  needles to  say, absolute
 junk.

I will write  here the absolute most important rules,  which should be
language independent.

The most important  rule, no, not a  rule a LAW is: every  rule can be
 broken, IFF you know what you are doing.

As a matter of fact, all norms, rules,  laws are done to help us to do
the right thing. If a rule hinder  us from doing the right thing, then
we could, no... we must break the rule.

The second most  important rule, often complety  forgotten or ignored:
write code for other humans. Yes,  you are writing for the computer to
compile it,  but most important  is that other people  understand your
code.

There is also,  related with the rule above, some  rules that are goog
practice to help making the code readable.

- Identifiers must be maximum 32 characters.  Note that if you have to
  use a  fully qualified name, the  whole name has to  be counted. The
  definition can be  tolerated to be longer, because it  does not come
  middle in the code.
- Lines should be maximum 80 characters
- Functions should be maximum 25 non empty lines, or 50 total lines
- Functions should have maximum 7 variables included input parameters
- Clases (or high level structures)  should have maximum 7 methods and
  7 properties (or 7 parts of each type)
- Each file should have maximum 1 class, or 7 functions, or 200 lines,
  or 10 kB
Maximum chaining of selection operators should be 3, and no repeated selector.
For example correct Name::Object->property.x is ok, but Name::Inside::Other is wrong. Also One->Two->Three or one.two.three are wrong.

If  at this  point  you  are questioning  yourself  why 7  everywhere:
https://en.wikipedia.org/wiki/The_Magical_Number_Seven,_Plus_or_Minus_Two

The  80  characters  per  line   comes  from  centuries  of  editorial
experience, where it have been proved already thousend times, that the
optimal  line   length  is   about  67  characters.   Considering  the
indentations  of any  programming  language 80  characters  is a  good
number. Note  that some people think  it has something to  do with old
monitors. They  are partly right, but  the truth is monitors  where 80
columns wide  because of the research  in optimal line length.  If you
want to check it, take a book  from a serious editorial, and count the
letters in each line: you will come  to a number around 70. The people
who  wrote the  google coding  standard  had no  idea why  it is  like
this.  This shows  google  coding standard  is by  no  means a  golden
standard.

Note: identifiers naming is a critical point. When you read a program,
what you  are going to  see is a sea  of reserved words,  literals and
identifiers.  The literals  are whatever  they must  be, the  reserved
words are defined, and you cannot do anything about them. You can only
define the identifiers.

The identifiers should always be named  in such a way that the program
reads as plain english. Because  anything alse, will make it difficult
to be read.

Specially bad is the so called "hungarian notation" of variables. This
is pure evil. Has absolutely no advance and makes reading difficult.

a_Do pn_you  v_think da_this s_text  v_is ad_easier pp_to  v_read c_if
pn_you s_signal  ip_what p_each s_word  v_is? Only somebody  with very
few programming experience can think this is a good idea.

Those are the important things. No what is NOT important: Formatting.

The  whole  formatting  or  style discussions,  outside  what  already
described here, are futile. Get a  frigging decent editor! (and a good
linter). So  people can checkout  the code,  format the code  how they
prefere to work  with it, and then  use the linter to save  it to some
standard format.

Pages and  pages of descriptions  of indenting practices,  ordering of
headers and  other crap can  be easily  avoided. Only people  who have
never used a decent editor  can spend months thinking about formatting
code. It  is year  2020 for  god's sake! a  computer can  do automatic
formatting! What a computer cannot still  do, is put sensible names to
identifiers.

Remember: clean code  is not "tidy" or "beautiful" code.  Is code that
can be easyly understood, and where errors can be easyly spotted.

